<?php
namespace LunetteBundle\Controller;
use LunetteBundle\Entity\lunette;
use LunetteBundle\Entity\marque;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CatalogController extends Controller{

    public function homeAction(){
        $marque=$this->getDoctrine()->getManager()->getRepository(marque::class)->findAll();
        return $this->render('@Lunette/Home.html.twig',array('marques'=>$marque));
    }

    public  function productAction(){
    return $this->render('@Lunette/Product.html.twig');
    }

}
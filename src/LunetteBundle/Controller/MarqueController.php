<?php

namespace LunetteBundle\Controller;

use LunetteBundle\Entity\marque;
use LunetteBundle\Form\marqueType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MarqueController extends Controller
{
    public function AddAction(Request $request)
    {
        $marque = new marque();
        $form= $this->createForm(marqueType::class,$marque);
        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            $file=$marque->getImage();
            $fileName= md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('image_directory'),$fileName
            );
            $marque->setImage($fileName);
            $em=$this->getDoctrine()->getManager();
            $em->persist($marque);
            $em->flush();
            return $this->redirectToRoute('showmarque');
        }
        return $this->render("@Lunette/Default/Addmarque.html.twig",array('form'=>$form->createView(),));
    }
    public function ShowAction()
    {
        $marque=$this->getDoctrine()->getManager()->getRepository(marque::class)->findAll();
        return $this->render("@Lunette/Default/Showmarque.html.twig",array('marques'=>$marque));
    }

    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $search =$request->query->get('search');
        $marque = $em->getRepository('LunetteBundle:marque')->findMulti($search);

        return $this->render('@Lunette/Default/Showmarque.html.twig', array(
            'marques' => $marque
        ));
    }

    public function ShowFrontAction()
    {
        $marque=$this->getDoctrine()->getManager()->getRepository(marque::class)->findAll();
        return $this->render('@Lunette/Catalog.html.twig',array('marques'=>$marque));
    }
}

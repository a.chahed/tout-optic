<?php

namespace AppBundle\Controller;

use LunetteBundle\Entity\marque;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $marque=$this->getDoctrine()->getManager()->getRepository(marque::class)->findAll();
        return $this->render('@Lunette/Home.html.twig',array('marques'=>$marque));

    }
}

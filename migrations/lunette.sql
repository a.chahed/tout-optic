-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 07 juil. 2019 à 21:27
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `lunette`
--

-- --------------------------------------------------------

--
-- Structure de la table `lunette`
--

DROP TABLE IF EXISTS `lunette`;
CREATE TABLE IF NOT EXISTS `lunette` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marqueid` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_851C42296D48642D` (`marqueid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `lunette`
--

INSERT INTO `lunette` (`id`, `marqueid`, `nom`, `image`, `prix`, `description`, `type`) VALUES
(5, 6, 'Ochialli', '055df46c34d7b48226ff7418c01c1c50.png', 420, 'Nouvelle collection', 'Optique'),
(6, 7, 'G200', '6f08bed9e5588094f31fccd9d3b1b3b1.png', 660, 'Summer collection', 'Optique'),
(7, 16, 'R 1525', '53048f85170a7da599b519de48c8ed3b.png', 530, 'Nouveau', 'lunette solaire'),
(8, 22, 'P707', 'a39b5315158ae8d93de3e4413e41c985.png', 400, '2019', 'lunette solaire'),
(9, 6, 'E150Y', '73703a1f2b6df58a6577e128030fa8c9.png', 530, 'White', 'lunette solaire'),
(10, 28, 'Zth500', 'e6a121336a64a9da9be552c941c11b06.jpeg', 250, 'Black', 'Optique'),
(11, 27, 'JAE72A9', '3f06b2fe41411c8d66d0b7b89efb5fa6.jpeg', 335, 'Blue', 'Optique');

-- --------------------------------------------------------

--
-- Structure de la table `marque`
--

DROP TABLE IF EXISTS `marque`;
CREATE TABLE IF NOT EXISTS `marque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `marque`
--

INSERT INTO `marque` (`id`, `image`, `nom`) VALUES
(6, '07620dc94bac182e8329c879c4e19a89.png', 'EXESS'),
(7, '25cd3a6cf888cb9fd469650c4a8ea258.png', 'GUESS / GUESS MARCIANO'),
(8, '497ed51ab5ea27a33f1c3923530c2acc.png', 'ROBERTO CAVALLI'),
(9, 'eb50f97ab8659ce054aea57c07ba5035.png', 'JUST CAVALLI'),
(10, 'c758dabd08c9b30a34d7d55f0d94f987.png', 'DSQUARED'),
(11, '24f9e83a7d549e6b5b7ef0bb7b44ab62.png', 'TOM FORD'),
(12, '556d924e8e06e074af371577a5d5783b.png', 'TOD\'S'),
(13, 'a16a48eaedb94a83d352cb455222fb28.png', 'POLAROID'),
(14, '4bf8318cf5a0aed4eb3be25c8cd26493.png', 'G-STAR'),
(15, 'c63a313ba05f75b58cd43feae803ccba.png', 'CARRERA'),
(16, '211502fe3fb37dd9e32063688cae8e5c.png', 'RAY-BAN'),
(18, '2ce5982844efd73402d54b2d30256604.png', 'JIMMY CHOO'),
(19, '960562a007a24465aee76476529ad0ff.png', 'LUI-JO'),
(20, '6ea59d68b4b7cd737387d42178ae3473.png', 'YSL/ Saint Laurent'),
(21, 'e7c53bb755bf9878b88889b805996574.png', 'FENDI'),
(22, 'de5b6f8ba25794ca5f7b7c1957abfa35.png', 'POLICE'),
(23, '517a17c0c6fe0f0355506b00a345a7b4.png', 'CALVIN KLEIN'),
(24, '6673bfde9a5025dcfb12134caf268df3.png', 'TOMMY'),
(25, '8b235c7abc360d8cdb4990b28e18c708.png', 'DIESEL'),
(26, '0990701e09bc7478a4aac813e2f26d2a.png', 'VALENTINO'),
(27, 'c2054c098affeeafc4dafc5046bdf2e0.png', 'JAEGER'),
(28, '76a29d2b91d44a9e34024feba268ff54.png', 'ZENITH'),
(29, '1aa0f2c55b11de66f26b2083ecc2378d.png', 'LAZER'),
(31, 'e1eb3aa083faef54e09ac44ae0bc5d71.png', 'HUGO BOSS/ BOSS ORANGE'),
(32, '1e0496619ea676a0625a5e68de94f4ac.png', 'EMILIO PUCCI'),
(33, '661e6f01cca5b6b8d55e69f7a4f2c0bd.png', 'FURLA'),
(34, '4bed1011be3d68006a3b5807ab6dd1a8.png', 'MOSCHINO'),
(35, 'e3bde33916145f0234246d2c25535906.png', 'SWAROVSKI'),
(36, '2fadae27f9aeffb760b6a67bca7055de.png', 'CAMEO'),
(37, '1859a4ac643c812b5c1afefb5f053398.png', 'Jacques Lamont'),
(38, '89b70ed57650aa8afb381abc3a24576d.png', 'MOSKI'),
(39, '1bc88c5c39aa0198342d8aabecf0c819.png', 'X-EYES');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solde` int(11) NOT NULL,
  `date_heure` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `nom`, `prenom`) VALUES
(3, 'mahmoud', 'mahmoud', 'mahmoud.benmabrouk@esprit.tn', 'mahmoud.benmabrouk@esprit.tn', 1, NULL, '$2y$13$Ao5nT6ks3HqGwRVD2lmUwuWpn9p6IqiO6wABlE/MpQ3kaDqUVLg3e', '2019-07-07 20:57:05', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', 'Mahmoud', 'Ben Mabrouk'),
(9, 'client', 'client', 'Client@client.tn', 'client@client.tn', 1, NULL, '$2y$13$tzFVfc8PLaODDGzKA0bG4uZqHvBpxtc82SgL8l8AQTAderV1gcV6.', '2019-07-07 20:54:19', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Client', 'Client'),
(10, 'neji', 'neji', 'nejioptic@gmail.com', 'nejioptic@gmail.com', 1, NULL, '$2y$13$waOw4f4RpXx7EOn48AM/9O42z3BsYVqa7iWgB6UmOGOE9W/CDQLT.', '2019-07-07 20:58:49', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', 'Neji', 'Mohamed');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `lunette`
--
ALTER TABLE `lunette`
  ADD CONSTRAINT `FK_851C42296D48642D` FOREIGN KEY (`marqueid`) REFERENCES `marque` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

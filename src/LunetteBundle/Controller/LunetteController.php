<?php

namespace LunetteBundle\Controller;

use LunetteBundle\Entity\lunette;
use LunetteBundle\Entity\marque;
use LunetteBundle\Form\Lunette1Type;
use LunetteBundle\Form\lunetteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LunetteController extends Controller
{
    public function AddLunetteAction(Request $request)
    {
        $lunette= new lunette();
        $form=$this->createForm(lunetteType::class,$lunette);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $file=$lunette->getImage();
            $fileName= md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('image_directory'),$fileName
            );
            $lunette->setImage($fileName);
            $em=$this->getDoctrine()->getManager();
            $em->persist($lunette);
            $em->flush();
            return $this->redirectToRoute('showlunette');
        }
        return $this->render('@Lunette/Default/Addlunette.html.twig',array('form'=>$form->createView(),));
    }
    public function ShowlunetteAction()
    {
        $lunettes=$this->getDoctrine()->getManager()->getRepository(lunette::class)->findAll();
        return $this->render('@Lunette/Default/ShowLunette.html.twig',array('lunettes'=>$lunettes));
    }
    public function DeleteLunetteAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $lunette=$em->getRepository(lunette::class)->find($id);
        $em->remove($lunette);
        $em->flush();
        return $this->redirectToRoute('showlunette');
    }
    public function UpdateLunetteAction($id,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $lunette=$em->getRepository(lunette::class)->find($id);
        $form=$this->createForm(Lunette1Type::class,$lunette);
        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            $file=$lunette->getImage();
            $fileName= md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('image_directory'),$fileName
            );
            $lunette->setImage($fileName);
            $em=$this->getDoctrine()->getManager();
            $em->persist($lunette);
            $em->flush();
            return $this->redirectToRoute('showlunette');
        }
        return $this->render("@Lunette/Default/Updatelunette.html.twig",array('form'=>$form->createView(),));
    }

    public function searchlunetteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $search =$request->query->get('search');
        $marque = $em->getRepository('LunetteBundle:lunette')->findMulti($search);

        return $this->render('@Lunette/Default/ShowLunette.html.twig', array(
            'lunettes' => $marque
        ));
    }

    public function ShowlunetteFrontAction()
    {
        $lunettes=$this->getDoctrine()->getManager()->getRepository(lunette::class)->findAll();
        $marques=$this->getDoctrine()->getManager()->getRepository(marque::class)->findAll();
        return $this->render('@Lunette/Catalog.html.twig',array('lunettes'=>$lunettes,'marques'=>$marques));
    }
    public function listbymarqueAction($critere)
    {
        $em=$this->getDoctrine()->getManager();
        $lunettes=$em->getRepository(lunette::class)->findBy(['marqueid'=>$critere]);
        //$lunettes=$em->getRepository(lunette::class)->findQuestionParameter($critere);
        $marques=$em->getRepository(marque::class)->findAll();
        return $this->render("@Lunette/Catalog.html.twig",array('lunettes'=>$lunettes,'marques'=>$marques));
    }
    public function listbytypeAction($critere)
    {
        $em=$this->getDoctrine()->getManager();
        $lunette=$em->getRepository(lunette::class)->findBy(['type'=>$critere]);
        $marque=$em->getRepository(marque::class)->findAll();
        return $this->render("@Lunette/Catalog.html.twig",array('lunettes'=>$lunette,'marques'=>$marque));
    }
    public function contactAction(){
        return $this->render('@Lunette/Contact.html.twig');
    }
}

<?php

namespace AppBundle\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use LunetteBundle\Entity\marque;

class secureController extends Controller
{
    /**
     * @Route("/redirect/")
     */
    public function RedirectAction()
    {
        $autochecker=$this->container->get('security.authorization_checker');
        if($autochecker->isGranted('ROLE_ADMIN'))
        {
            return $this->render('@Lunette/Default/index.html.twig');
        }
        elseif($autochecker->isGranted('ROLE_CLIENT'))
        {
            $marque=$this->getDoctrine()->getManager()->getRepository(marque::class)->findAll();
            return $this->render('@Lunette/Home.html.twig',array('marques'=>$marque));

        }
        else
        {
            return $this->render('@FOSUser/Security/login.html.twig');
        }
    }
}
